# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import traefik with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set mountpoint = traefik.docker.mountpoint %}
{%- set letsencrypt_email = traefik.get('letsencrypt_email', 'test@example.com') %}

{%- for dir in [
  'docker/traefik/config',
  'docker/traefik/letsencrypt'
  ]
%}

traefik-config-file-{{ dir|replace('/', '_') }}-directory-managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}

traefik-config-file-traefik-global-config-managed:
  file.managed:
    - name: {{ mountpoint }}/docker/traefik/config/traefik.yaml
    - source: {{ files_switch(['traefik.yaml.tmpl'],
                              lookup='traefik-config-file-traefik-global-config-managed',
                 )
              }}
    - user: stooj
    - group: users
    - mode: 644
    - makedirs: True
    - template: jinja
    - context:
        letsencrypt_email: {{ letsencrypt_email }}
