# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import traefik with context %}
{%- set sls_config_file = tplroot ~ '.config.file' %}

{%- set mountpoint = traefik.docker.mountpoint %}
{%- set do_auth_token = traefik.get('do_auth_token', '12345') %}

include:
  - {{ sls_config_file }}

traefik-container-running-image-present:
  docker_image.present:
    - name: traefik
    - tag: v2.3
    - force: True

traefik-container-running-container-managed:
  docker_container.running:
    - name: traefik
    - image: traefik:v2.3
    - restart: always
    - binds:
      - {{ mountpoint }}/docker/traefik/config:/etc/traefik/
      - {{ mountpoint }}/docker/traefik/certificates:/letsencrypt
      - /var/run/docker.sock:/var/run/docker.sock
    - port_bindings:
      - 8080:8080
      - 80:80
      - 443:443
    - watch:
      - sls: {{ sls_config_file }}
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
      - DO_AUTH_TOKEN: {{ do_auth_token }}
